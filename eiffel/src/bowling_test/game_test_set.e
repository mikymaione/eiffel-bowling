	--MIT License
	--Copyright (c) 2019 Michele Maione
	--Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
note
	description: "Testing class"
	author: "[MAIONE MIKY]"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit

	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE} -- Initialization

	on_prepare
		do
			create g.make
		end

feature -- Test routines

	g: GAME

	testGutterGame()
		do
			rollMany (20, 0)
			assert ("testGutterGame", 0 = g.punti())
		end

	testAllOnes()
		do
			rollMany (20, 1)
			assert ("testAllOnes", 20 = g.punti())
		end

	testOneSpare()
		do
			rollSpare()
			g.roll (3)
			rollMany (17, 0)
			assert ("testOneSpare", 16 = g.punti())
		end

	testOneStrike()
		do
			rollStrike()
			g.roll (3)
			g.roll (4)
			rollMany (16, 0)
			assert ("testOneStrike", 24 = g.punti())
		end

	testPerfectGame()
		do
			rollMany (12, 10)
			assert ("testPerfectGame", 300 = g.punti())
		end

	testLastSpare()
		do
			rollMany (9, 10)
			rollSpare()
			g.roll (10)
			assert ("testLastSpare", 275 = g.punti())
		end

	rollSpare()
		do
			g.roll (5);
			g.roll (5);
		end

	rollStrike()
		do
			g.roll (10)
		end

	rollMany (n, pins: INTEGER)
		local
			i: INTEGER
		do
			from
				i := 0
			until
				not (i < n)
			loop
				g.roll (pins)
				i := i + 1
			end
		end

end
