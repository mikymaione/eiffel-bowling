	--MIT License
	--Copyright (c) 2019 Michele Maione
	--Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
note
	description: "Bowling!"
	author: "[MAIONE MIKY]"

class
	HELLO

create
	make

feature

	make
		do
			partita_perfetta
		end

	partita_perfetta
		local
			g: GAME
			i: INTEGER
		do
			create g.make

			from
				i := 0
			until
				not (i < 12)
			loop
				g.roll (10)
				i := i + 1
			end
		end

	nuova_partita
		local
			orario: TIME
			g: GAME
			r: RANDOM
			t: STRING
			maxt, rndI, i, x: INTEGER
			tiri: ARRAYED_LIST [INTEGER]
		do
			maxt := 10

			create g.make
			create orario.make_now
			create r.set_seed (orario.minute * orario.second + orario.milli_second)
			create tiri.make (maxt)

			from
				i := 1
			until
				not (i < maxt)
			loop
				r.forth
				rndI := r.item \\ 10 + 1
				g.roll (rndI)
				tiri.put_front (rndI)
				i := i + 1
			end

			t := "Partita bowling%N";

			across
				tiri as punto
			loop
				x := x + 1
				t := t + x.out + "# " + punto.item.out + "%N"
			end

			t := t + "%NScore: " + g.punti().out + "%N"

			io.put_string (t);
			io.put_new_line
		end

end
