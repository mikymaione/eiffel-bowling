	--MIT License
	--Copyright (c) 2019 Michele Maione
	--Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
note
	description: "Summary description for {GAME}."
	author: "[MAIONE MIKY]"

class
	GAME

create
	make

feature {NONE}

	rolls: ARRAY [INTEGER]

	currentRoll_: INTEGER

feature {ANY}

	make
		do
			create rolls.make_filled (0, 1, 21)
		end

	CurrentRoll: INTEGER
		do
			result := currentRoll_
		end

	roll (birilli_a_terra: INTEGER)
		require
			birilli_a_terra >= 0
			birilli_a_terra <= 10
		do
			currentRoll_ := currentRoll_ + 1
			rolls [currentRoll] := birilli_a_terra
		end

	punti: INTEGER
		local
			i, score, frame: INTEGER
		do
			frame := 1

			from
				i := 1
			until
				not (i < 10)
			loop
				if isStrike (frame) then
					score := score + 10 + strikeBonus (frame)
					frame := frame + 1
				elseif isSpare (frame) then
					score := score + 10 + spareBonus (frame)
					frame := frame + 2
				else
					score := score + sumOfRolls (frame)
					frame := frame + 2
				end
				
				i := i + 1
			end

			result := score
		ensure
			result >= 0
			result <= 300
		end

	isStrike (frame: INTEGER): BOOLEAN
		require
			frame >= CurrentRoll
		do
			result := rolls [frame] = 10
		end

	isSpare (frame: INTEGER): BOOLEAN
		require
			frame >= CurrentRoll
		do
			result := sumOfRolls (frame) = 10
		end

	strikeBonus (frame: INTEGER): INTEGER
		require
			frame >= CurrentRoll
		do
			result := sumOfRolls (frame + 1)
		end

	spareBonus (frame: INTEGER): INTEGER
		require
			frame >= CurrentRoll
		do
			result := rolls [frame + 2]
		end

	sumOfRolls (frame: INTEGER): INTEGER
		require
			frame >= CurrentRoll
		do
			result := rolls [frame] + rolls [frame + 1]
		end

end
